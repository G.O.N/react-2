import React from 'react';

class UserDataTable extends React.Component {
  constructor(props){
    super(props);

    this.getHeader = this.getHeader.bind(this);
    this.getRowsData = this.getRowsData.bind(this);
    this.getKeys = this.getKeys.bind(this);
  }
    
    getKeys = function(){
      return Object.keys(this.props.users);
    }
    
    getHeader = function() {

      var keys = this.getKeys();

      return keys.map(
        (key, index) => {
          return <th key={key}>{key.toUpperCase()}</th>
        }
      );
    }
    
    getRowsData = function() {
      var items = this.props.users;
      var keys = this.getKeys();
      return items.map((row, index) => {
        return <tr key={index}>
          <th scope="row">{index + 1}</th>
          <td>{row.username}</td>
          <td>{row.password}</td>
          <td>{row.name}</td>
          <td>
            <a href={row.id} className="btn btn-danger cil-delete">Delete</a>
          </td>
          </tr>
      })
    }
  
  render() {
    return (
     <div>
        <CDataTable className="table table-striped table-hover">
          <thead className="thead">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Username</th>
              <th scope="col">Password</th>
              <th scope="col">Name</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            {this.getRowsData()}
          </tbody>
        </table>
      </div>
    );
  }
}



export default UserDataTable;
