from fastapi import FastAPI
from app.api.users import users
from app.api.db import metadata, database, engine
from starlette.middleware.cors import CORSMiddleware

metadata.create_all(engine)

app = FastAPI()

origins = [
    "http://localhost:3000/",
    "http://localhost:8000/"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"]
)

@app.on_event("startup")
async def startup():
    await database.connect()

@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


app.include_router(users)