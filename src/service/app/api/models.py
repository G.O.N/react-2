from pydantic import BaseModel
from typing import List, Optional
from sqlalchemy.orm import validates

class UserIn(BaseModel):
    username: str
    password: str
    name: str

    @validates('username')
    def validate_username(self, key, username):
        min_length = 3
        max_length = 100
        if not username:
            raise AssertionError('username cannot be blank')
        if len(username) < min_length or len(username) > max_length:
            raise AssertionError('username must be between ' + min_length +' and ' + max_length +' characters long')

    @validates('username')
    def validate_username(self, key, username):
        min_length = 7
        max_length = 100
        if not password:
            raise AssertionError('password cannot be blank')
        if len(password) < min_length or len(password) > max_length:
            raise AssertionError('password must be between ' + min_length +' and ' + max_length +' characters long')

    @validates('name')
    def validate_name(self, key, name):
        min_length = 3
        max_length = 100
        if not name:
            raise AssertionError('name cannot be blank')
        if len(name) < min_length or len(name) > max_length:
            raise AssertionError('name must be between ' + min_length +' and ' + max_length +' characters long')


class UserOut(UserIn):
    id: int


class UserUpdate(UserIn):
    username: Optional[str] = None
    password: Optional[str] = None
    name: Optional[str] = None