# User Management


## Pengenalan


Aplikasi ini terdiri atas 2 bagian:

1. Layanan API yang dibuat dengan dengan Python

    Pastikan sudah ter-install:

    - Python 3

    - PIP

2. Aplikasi client yang dibuat dengan React

    Pastikan sudah ter-install:

    - NodeJS

    - React

    - Yarn

## Menjalankan Layanan API

1. Pastikan MySQL telah terpasang dan berjalan

2. Buat database untuk menampung data

3. Set enviroment variable untuk menjadi acuan aplikasi menggunakan database, formatnya:

   Pada Bash:

   ```bash
   export DATABASE_URL="mysql+pymysql://nama_akun:password_akun@alamat_host_database/nama_database
   ```

   Pada PowerShell:

   ```powershell
   $env:DATABASE_URL="mysql+pymysql://nama_akun:password_akun@alamat_host_database/nama_database
   ```

4. Masuk ke folder src/service

5. Penuhi kebutuhan aplikasi dengan menjalankan perintah:

   ```
   pip install -r requirements.txt
   ```

6. Jalankan layanan API dengan perintah:

   ```bash
   uvicorn app.main:app --reload
   ```

    Perintah ini juga akan membuat tabel users pada database yang ditunjuk jika tabel tersebut belum ada.

7. Secara default layanan API bisa diakses pada http://localhost:8000/user

8. Dokumentasi swagger dari layanan API bisa diakses pada http://localhost:8000/docs

9. Halaman swagger juga menyediakan daftar API dalam format Json OpenAPI, yang bisa digunakan untuk di import kedalam aplikasi Postman dengan mudah. Json tersebut bisa di akses pada http://localhost:8000/openapi.json

## Menjalankan Aplikasi Client

1. Masuk ke folder src/client

2. Penuhi kebutuhan aplikasi dengan menjalankan perintah:

   ```bash
   yarn install
   ```

3. Jalankan layanan aplikasi dengan perintah

   ```bash
   yarn start
   ```

4. Secara default, aplikasi bisa diakses pada http://localhost:3000
